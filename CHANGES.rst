0.1
---

Initial release, with a basic feature-set to run each test in its own sandbox:

* host file system is mounted read-only, CWD is mounted read-write;
* network access is disabled by default but can be enabled for selected tests;
* additional directories can be made read-only;
